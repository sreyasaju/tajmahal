# THE CROWN OF PALACES - THE TAJ MAHAL

A presentation on the **Taj Mahal** made on Reveal.js. This presentation gives some information on the Taj Mahal's geographical location, its architect , who commissioned it and for whom, its threats, some myths,  it's exterior and interior decorations and of course, the tombs in it. 





<hr> <p align="center">Created by <strong>Sreya Saju</strong></p>




